/**
 * Core Javascript
 */

var CB = {}; //Namespace for our Task

CB.Api = {}; //ToDo: define API URL base and stuff...

CB.Lang = {
    close: 'Close', 
    remove: 'Remove',
    loading: 'Loading...',
    ajaxError: 'Request error, please try again',




};

//Background loader that is shown mostly when loading new content or during ajax requests
CB.BackgroundLoader = function(displayAction, loaderText) {
    $('strong#background-loader-msg').text(loaderText);
    $('div#background-loader').css('display', displayAction);
}

//Simply alerts errors
CB.ErrorAlert = function(message) {
    alert(message);
}

/** 
 * Ajax related...
 */
CB.Ajax = {};

CB.Ajax.ErrorCallback = function(xhr, status, thrownError) {
	//console.log('! Error: ' + xhr);
	console.log('! Error: ' + status);
	console.log('! Error: ' + thrownError);
	CB.ErrorAlert(CB.Lang.ajaxError);
}

CB.Ajax.CompleteCallback = function() {
	CB.BackgroundLoader('none', '');
}

//Ajax request base
CB.Ajax.AbstractRequest = function(url, method, params, returnType, successCallback) {
	//if (typeof method === 'undefined') method = 'GET';
	//time=new Date().getTime()

	//var urlBase = task_config.baseUri;
	if (typeof returnType === 'undefined') {
		returnType = 'html';
	}
	/*if (typeof errorCallback === 'undefined') {
		errorCallback = CB.Ajax.ErrorCallback(xhr, status, thrownError);
	}
	if (typeof completeCallback === 'undefined') {
		completeCallback = CB.Ajax.CompleteCallback();
	}*/

	CB.BackgroundLoader('block', CB.Lang.loading);
	$.ajax({
	    type: method,
	    url: url,
	    data: params,
	    dataType: returnType,
	    success: successCallback,
	    error: function(xhr, status, thrownError) {
	    	CB.Ajax.ErrorCallback(xhr, status, thrownError);
	    },
	    complete: function() {
			CB.Ajax.CompleteCallback();
	    }
	});
}

//Get request
CB.Ajax.Get = function(url, params, successCallback, returnType) {
	CB.Ajax.AbstractRequest(url, 'GET', params, returnType, successCallback);
}

//Post request
CB.Ajax.Post = function(url, params, successCallback, returnType) {
	CB.Ajax.AbstractRequest(url, 'POST', params, returnType, successCallback);
}

